REGION=$1
TAG=$2
ROLE=$3

echo "===== checking for tag and presence in main branch ====="
([ -z $TAG ] || (git branch -r --contains $(git rev-list -n 1 $TAG) | grep main))
echo "===== assuming permissions => $ROLE ====="
eval $(aws sts assume-role --role-arn $ROLE --role-session-name "deployment" | jq -r '.Credentials | "export AWS_ACCESS_KEY_ID=\(.AccessKeyId)\nexport AWS_SECRET_ACCESS_KEY=\(.SecretAccessKey)\nexport AWS_SESSION_TOKEN=\(.SessionToken)\n"')
export AWS_DEFAULT_REGION=$REGION
