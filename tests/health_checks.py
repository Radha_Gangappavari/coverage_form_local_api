from src.db_manager import db
from src.server import create_app
import pytest

app = create_app()


@pytest.fixture
def tester():
    with app.test_client() as client:
        yield client


def test_index(tester):
    response = tester.get("/api/v1/attributes_test")
    assert response.status_code == 200


def test_index_content(tester):
    response = tester.get("/api/v1/attributes_test")
    assert response.content_type == "application/json"


def test_index_data(tester):
    response = tester.get("/api/v1/attributes_test")
    assert len(response.data) > 0
