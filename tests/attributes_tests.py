# run from main folder:
# python -m pytest tests/attributes_tests.py
# python -m coverage run tests/attributes_tests.py
# python -m coverage report >> coverage_report.txt
import json
import sys
import os
# Determine the root directory path and append it to sys.path
root_path = os.path.abspath(os.path.join(os.path.dirname(__file__), '..'))
sys.path.append(root_path)

from resources import attr_blp
from models import LkpRatingAttributeModel as RatingAttributeModel
from load_environment import *  # Import the load_environment module from the root directory
from src.db_manager import db

# create random number and convert it to string
def random_number():
    import random
    return str(random.randint(1, 100000))

def test_get_attribute_positive(client, app):
    rnd_num = random_number()
    with app.app_context():
        example_attribute = RatingAttributeModel(
            IDT_TEST_TBL_LKP_RATING_ATTRIBUTE=rnd_num,
            CDE_ATTRIBUTE=rnd_num,
            DES_ATTRIBUTE="test_description",
            TXT_DATATYPE="test_datatype",
        )
        db.session.add(example_attribute)
        db.session.commit()

    response = client.get("/api/v1/attribute/" + rnd_num)
    assert response.status_code == 200
    data = json.loads(response.data)
    assert data["CDE_ATTRIBUTE"] == rnd_num
    assert data["DES_ATTRIBUTE"] == "test_description"
    assert data["TXT_DATATYPE"] == "test_datatype"

def test_get_attribute_negative(client):
    rnd_num = random_number() + "1"
    response = client.get("/api/v1/attribute/test")
    assert response.status_code == 404

def test_post_attribute_positive(client, app):
    rnd_num = random_number()
    attribute_data = {
        "IDT_TEST_TBL_LKP_RATING_ATTRIBUTE": rnd_num,
        "CDE_ATTRIBUTE": rnd_num,
        "DES_ATTRIBUTE": "new_description",
        "TXT_DATATYPE": "new_datatype",
    }
    response = client.post("/api/v1/attributes", json=attribute_data)
    assert response.status_code == 201
    data = json.loads(response.data)
    assert data == "success"

    with app.app_context():
        new_attribute = RatingAttributeModel.query.filter_by(
            CDE_ATTRIBUTE=rnd_num
        ).first()
        assert new_attribute is not None
        assert new_attribute.DES_ATTRIBUTE == "new_description"
        assert new_attribute.TXT_DATATYPE == "new_datatype"

def test_post_attribute_negative(client):
    attribute_data = {
        "CDE_ATTRIBUTE": random_number(),
        # Missing "DES_ATTRIBUTE" and "TXT_DATATYPE"
    }
    response = client.post("/api/v1/attributes", json=attribute_data)
    assert response.status_code == 422
    
