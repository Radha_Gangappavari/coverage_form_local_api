import os
from flask import Flask
from src.db_manager import db
from resources import attr_blp
import pytest

@pytest.fixture
def app():
    app = Flask(__name__)
    app.config["TESTING"] = True
    app.config["SQLALCHEMY_DATABASE_URI"] = os.getenv("DATABASE_URL")
    app.register_blueprint(attr_blp)

    db.init_app(app)

    return app

@pytest.fixture
def client(app):
    return app.test_client()
