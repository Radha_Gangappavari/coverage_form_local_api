from db import db
from sqlalchemy import Table, MetaData, Column, Integer, Float, String, DateTime
from sqlalchemy.ext.declarative import declarative_base
# import environment variables
import os
from dotenv import load_dotenv
from datetime import datetime
from pydantic import BaseModel, Field, validator

# import env variables
load_dotenv()

metadata = MetaData()

'''
a model is a mapping between a row in the table 
and a python object 
''' 
class TblLookupClassModel(db.Model): 
    __tablename__= 'tbl_lookup_class'
    __table_args__={'schema': os.getenv('DB_SCHEMA')}

    CDE_CLASS = db.Column(db.String, primary_key = True)
    NME_CLASS = db.Column(db.String, nullable=False)
    CDE_ISO_CLASS = db.Column(db.String, nullable=False)
    CDE_EXP_BASE = db.Column(db.String, nullable=False)
    NUM_DIVISOR = db.Column(db.Integer, nullable=False)
    IND_ACTIVE = db.Column(db.Integer, nullable=False)
    DTE_MOD = db.Column(db.DateTime, nullable=False)
    UID_MOD = db.Column(db.String, nullable=False)
    NME_CLASS_RPT = db.Column(db.String, nullable=False)
    NUM_HAZARD = db.Column(db.Integer, nullable=False)