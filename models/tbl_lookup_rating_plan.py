from db import db
from sqlalchemy import Table, MetaData, Column, Integer, Float, String, DateTime
from sqlalchemy.ext.declarative import declarative_base
# import environment variables
import os
from dotenv import load_dotenv
from datetime import datetime
from pydantic import BaseModel, Field, validator

# import env variables
load_dotenv()

metadata = MetaData()

'''
a model is a mapping between a row in the table 
and a python object 
''' 
class TblLookupRatingPlanModel(db.Model): 
    __tablename__= 'tbl_lookup_rating_plan'
    __table_args__={'schema': os.getenv('DB_SCHEMA')}

    CDE_RATING_PLAN = db.Column(db.String, primary_key=True)
    NME_RATING_PLAN = db.Column(db.String, nullable=False)
    IND_ACTIVE = db.Column(db.Integer, primary_key=True)
    DTE_MOD = db.Column(db.DateTime, nullable=False)
    UID_MOD = db.Column(db.String, nullable=False)