from db import db
from sqlalchemy import Table, MetaData, Column, Integer, Float, String, DateTime
from sqlalchemy.ext.declarative import declarative_base
# import environment variables
import os
from dotenv import load_dotenv
from datetime import datetime
from pydantic import BaseModel, Field, validator

# import env variables
load_dotenv()

metadata = MetaData()

'''
a model is a mapping between a row in the table 
and a python object 
''' 
class TblRtgDataClass(db.Model): 
    __tablename__= 'tbl_rtg_data_class'
    __table_args__={'schema': os.getenv('DB_SCHEMA')}

    idt_data_class = db.Column(db.Integer, primary_key=True)
    num_ref_class = db.Column(db.Integer, nullable=False)
    cde_class = db.Column(db.String, nullable=False)
    NUM_BASE_RATE = db.Column(db.Integer, nullable=False)
    NUM_FCTR_SEVERITY = db.Column(db.Integer, primary_key=True)
    NUM_REF_SIZE = db.Column(db.Integer, primary_key=True)
    NUM_REF_TERRITORY = db.Column(db.Integer, primary_key=True)
    NUM_REF_HZD_GROUP = db.Column(db.Integer, primary_key=True)
    NUM_REF_DEDUCTIBLE = db.Column(db.Integer, primary_key=True)
    NUM_REF_CLAIMS_MADE = db.Column(db.Integer, primary_key=True)
    AMT_PREM_MIN = db.Column(db.Integer, primary_key=True)
    CDE_XRW = db.Column(db.String, nullable=False)
    TXT_XRW = db.Column(db.String, nullable=False)
    DTE_MOD = db.Column(db.DateTime, nullable=False)
    UID_MOD = db.Column(db.String, nullable=False)
    NUM_CHANGE_ID = db.Column(db.Integer, primary_key=True)
    DTE_APPROVED = db.Column(db.DateTime, nullable=False)
    DTE_EXPIRED = db.Column(db.DateTime, nullable=False)
    NUM_EXPIRED_BY = db.Column(db.Integer, primary_key=True)
    IDT_REF_COMM_CREDIT = db.Column(db.Integer, primary_key=True)
