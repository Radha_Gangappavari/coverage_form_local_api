from db import db
from sqlalchemy import Table, MetaData, Column, Integer, Float, String, DateTime
from sqlalchemy.ext.declarative import declarative_base
# import environment variables
import os
from dotenv import load_dotenv
from datetime import datetime
from pydantic import BaseModel, Field, validator

# import env variables
load_dotenv()

metadata = MetaData()

'''
a model is a mapping between a row in the table 
and a python object 
''' 
class TblXrefWkshtCfrmCovgModel(db.Model): 
    __tablename__= 'tbl_xref_wksht_cfrm_covg'
    __table_args__={'schema': os.getenv('DB_SCHEMA')}

    IDT_XREF_WKSHT_CFRM_COVG = db.Column(db.Integer, primary_key=True)
    CDE_BUS_UNIT = db.Column(db.String, nullable=False)
    CDE_RATING_PLAN = db.Column(db.String, nullable=False)
    CDE_COVG_FORM = db.Column(db.String, nullable=False)
    DTE_MOD = db.Column(db.DateTime, nullable=False)
    UID_MOD = db.Column(db.String, nullable=False)
    CDE_COVERAGE = db.Column(db.String, nullable=False)
    PCT_PREM_SPLIT = db.Column(db.Integer, nullable=False)
    CDE_LMT_GEN_AGG = db.Column(db.String, nullable=False)
    CDE_LMT_OCCURRENCE = db.Column(db.String, nullable=False)
    CDE_LMT_PROD_AGG = db.Column(db.String, nullable=False)
    CDE_LMT_MED_EXP = db.Column(db.String, nullable=False)
    CDE_LMT_FIRE_LGL = db.Column(db.String, nullable=False)
    CDE_LMT_PERS_ADV = db.Column(db.String, nullable=False)
    CDE_DED_OCCURRENCE = db.Column(db.String, nullable=False)
    CDE_DED_AGGREGATE = db.Column(db.String, nullable=False)
    CDE_SUBLINE = db.Column(db.String, nullable=False)
    NUM_DED_REF = db.Column(db.Integer, nullable=False)
    