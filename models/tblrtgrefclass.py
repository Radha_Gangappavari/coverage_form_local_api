  
from db import db
from sqlalchemy import MetaData, Column, Integer, String
from sqlalchemy.dialects.oracle import CLOB


# import environment variables
import os
from dotenv import load_dotenv
from datetime import datetime
from pydantic import BaseModel, Field, validator

# import env variables
load_dotenv()

metadata = MetaData()


"""
a model is a mapping between a row in the table 
and a python object 
"""


class TblRtgRefClassModel(db.Model):
    __tablename__ = "tbl_rtg_ref_class"
    __table_args__ = {"schema": os.getenv("DB_SCHEMA")}

    IDT_REF_CLASS = db.Column(db.Integer, primary_key=True)
    NME_REF_CLASS = db.Column(db.String(250))
    dte_mod =  db.Column(db.DateTime, nullable=False)
    uid_mod = db.Column(db.String, nullable=False)
    NUM_CHANGE_ID = db.Column(db.Integer, nullable=False)
    autoload = True
