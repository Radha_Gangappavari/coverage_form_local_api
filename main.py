import signal
import logging
import os
from src.server import create_app, start_server

# Ensure the 'logs' directory exists
log_dir = 'logs'
if not os.path.exists(log_dir):
    os.makedirs(log_dir)

# Create or append to the log file
log_file_path = os.path.join(log_dir, 'app.log')

# Configure the logger
logger = logging.getLogger(__name__)
logging.basicConfig(filename=log_file_path, level=logging.INFO, filemode='a', format='%(asctime)s - %(levelname)s - %(message)s')

# Signal handler function
def handle_termination(signal, frame):
    logger.info("Shutting down Waitress server...")
    raise SystemExit

# Register the handler for termination signals
signal.signal(signal.SIGINT, handle_termination)

if __name__ == '__main__':
    app = create_app()
    start_server(app)
