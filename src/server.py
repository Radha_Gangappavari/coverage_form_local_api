from src.db_manager import initialize_db

from waitress import serve
import logging
from src.config import Config
from flask import Flask
from flask_smorest import Api

# import the blueprint from resources
from resources import (
    attr_blp,
    def_blp,
    db_uptime_blp,
    demo_get_blp,
    demo_post_blp,
    flush_cache_blp,
)


# Initialize logger for the server
logger = logging.getLogger(__name__)


# factory patterns
def create_app(db_url=None):
    app = Flask(__name__)
    app.config.from_object(Config) 

    # initialize the database
    initialize_db(app)

    print("database url: ", db_url)

    api = Api(app)

    # register the blueprint
    api.register_blueprint(attr_blp)
    api.register_blueprint(def_blp)
    api.register_blueprint(db_uptime_blp)
    api.register_blueprint(demo_get_blp)
    api.register_blueprint(flush_cache_blp)
    api.register_blueprint(demo_post_blp)

    return app


# Start the server
def start_server(app):
    waitress_thread_count = 4
    logger.info(
        f"Starting Waitress server on 0.0.0.0:5000 with {waitress_thread_count} threads..."
    )
    serve(app, host="0.0.0.0", port=5000, threads=waitress_thread_count)
