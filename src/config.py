from load_environment import *  # Import the load_environment module from the root directory

import sys
import os
from sqlalchemy.pool import QueuePool

sys.path.append("..")  # Add the parent directory (root) to the system path

# Configuration for the Flask application
class Config(object):
    PROPAGATE_EXCEPTIONS = True
    API_TITLE = "Attributes API"
    API_VERSION = os.getenv("API_VERSION", "v1")
    OPENAPI_VERSION = "3.0.2"
    OPENAPI_URL_PREFIX = "/api"
    OPENAPI_SWAGGER_UI_PATH = "/swagger-ui"
    OPENAPI_SWAGGER_UI_URL = "https://cdnjs.cloudflare.com/ajax/libs/swagger-ui/3.24.2/" 
    OPENAPI_SWAGGER_UI_VERSION = "3.24.2"
    SQLALCHEMY_DATABASE_URI = os.getenv("DATABASE_URL")
    # SQLALCHEMY_DATABASE_URI = "oracle+oracledb://TMAYFIELD:C0me0nS0n,Psych!*09@isubm-dev.nps.kinsale.cloud:1521/isubm"
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    SQLALCHEMY_ENGINE_OPTIONS = {
        "poolclass": QueuePool,
        "pool_pre_ping": True,
        "pool_size": 10,
        "max_overflow": 20,
    }