from src.db import db

import logging
from sqlalchemy.pool import QueuePool

# Initialize logger for the database manager
logger = logging.getLogger(__name__)


# Class for counting connections
class ConnectionCounter(object):
    def __init__(self):
        self.count = 0

    def checkout(self, dbapi_con, con_record, con_proxy):
        self.count += 1
        logger.info("A connection was checked out, total connections: %s", self.count)

    def checkin(self, dbapi_con, con_record):
        self.count -= 1
        logger.info("A connection was checked in, total connections: %s", self.count)


# An instance of the ConnectionCounter class is created here
counter = ConnectionCounter()


# Initialize and configure the database
def initialize_db(app):
    db.init_app(app)

    with app.app_context():
        # Listen for connection events
        # The 'counter' object is used here to count the number of connections
        db.event.listen(db.engine, "checkout", counter.checkout)
        db.event.listen(db.engine, "checkin", counter.checkin)
