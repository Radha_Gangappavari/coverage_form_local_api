FROM python:3.11

WORKDIR /api 

COPY . .

RUN pip install -r requirements.txt  && \
    mkdir -p /api/logs

EXPOSE 5000

CMD ["python", "main.py", "--host", "0.0.0.0"]

ENV TCP_KEEPIDLE 30

# docker build -t flask-demo-app .
# docker run -p 5005:5000 flask-demo-app

# add volume - allows the app to refresh without rebuilding the image 
# docker run -dp 5000:5000 -w /app -v "$(pwd):/app" flask-demo-app

# we will add a sidecar/agent to the container to ship the logs to a log aggregator (talk to Bryce)

# create docker network
# docker network create flask-redis

# docker run --name redisdockernw --network=flask-redis -d redis
# docker run --name flask-demo-app --network=flask-redis -d -p 5000:5000 -e REDIS_HOST=redisdockernw <IMAGE NAME>