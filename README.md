# Data Access Layer (DEMO)

## What is Flask?

Flask is a micro web framework written in Python. It is classified as a microframework because it does not require particular tools or libraries. 
It has no database abstraction layer, form validation, or any other components where pre-existing third-party libraries provide common functions. 
However, Flask supports extensions that can add application features as if they were implemented in Flask itself. Extensions exist for object-relational mappers, 
form validation, upload handling, various open authentication technologies and more.

## Getting started

Create a virtual environment - its best to operate within a venv for this project 

Steps: 
    1. Install the virtual environment: python -m venv venv

    2. Navigate to the newly created Scripts folder under the venv folder: cd venv > Scripts

    3. From there, activate the virtual environment: source activate
    
    4. You should see the (venv) tag in your git bash window 

Install all libraries in the core requirements.txt file. This should be done from within the virtual environment. 

```
pip install -r requirements.txt
```

## Working within Docker

Steps for getting started:
1. Download Docker Desktop

2. Create a Dockerfile file at the root level of the project. 

3. The Dockerfile should contain instructions for adding project components and running the main file
    ```
    FROM python:3.11
    WORKDIR /api 
    COPY . .
    RUN pip install -r requirements.txt
    EXPOSE 5000
    CMD ["python", "main.py", "--host", "0.0.0.0"]
    ENV TCP_KEEPIDLE 30
    ```

4. This project uses Redis. Redis will need to be deployed in a separate Docker file with an image built using docker-compose. 
If you are running both the flask app, and the Redis app in Docker, you will need to create a network between the two docker images. 
The Redis container should only be used for testing. In production, the app will connect to a production instance of Redis. 
    ```
    docker network create my-network
    docker run -d --name container1 --network my-network image1
    docker run -d --name container2 --network my-network image2
        ```

# Starting the App

1. Make sure Redis is running in a Docker file or in a "sidecar" in the same container/pod 

2. Run the following from the root folder (git bash): python -m main

# Notes

Attributes and Definitions are example objects that are used in this demo project. 
In real domain-oriented services, appropriate object or table-level naming will be used. 

## Terminology

MARSHMALLOW: The Marshmallow library in Python is a popular library used for object serialization/deserialization. 
Serialization is the process of converting a data structure or object state into a format that can be stored or transmitted and then recreated.
In simple terms, Marshmallow provides a simple way to:

        - Convert complex data types, like objects, to Python data types (serialization).
        - Convert Python data types to complex data types (deserialization).
        - Validate data.

BLUEPRINT: A Flask Blueprint is essentially a way for you to organize your Flask application into smaller and reusable applications. 
Blueprints are used for defining reusable functions, templates, static files, etc., which can be used across multiple 
applications or in different parts of the same application.

    For example, you might have a set of routes and view functions that are related to user management (like login, logout, and profile). 
    You could put these into a Blueprint, and then you can use that Blueprint in your main application, or even in other Flask applications.

## Coding Practices 

Return Values:
It's good practice to have consistent return types for functions that can fail.  Ensure the calling functions can handle  None values properly.

Exception Handling (try/catch):
Assume that any service that is being used can fail. For example, always put a resilient wrapper around code that is cached. 
Assume that if Redis goes down, the get processes must still work. 

Inheritance:
Define methods in the base class that are common across all derived classes and then add or override methods in derived classes as needed.

Interfaces:
Interfaces should be stable, consistent, and well-documented, as changes to public interfaces can lead to cascading updates 
in all the parts of the codebase that rely on them.


## Folders and Files 

CONNECTIONS: SQLAlchemy database reference

MODELS: Data models that map to objects in Oracle. One database objects per file. 

RESOURCES: Blueprints, which is in-effect the "REPOSITORY" of the Repository Pattern. The data-abstracted API 
interfaces are stored in the RESOURCES folder. One API interface that maps to one or more tables per file. 

SCHEMA: Marshmallow-enabled validation rules that map to database constraints. One schema per file. 

## Designing a RESTful API 

When creating an API, you'll often want to follow the REST principles. This means that you'll create different routes 
that correspond to different resources, and then on those routes, you'll define what happens when you use 
different HTTP methods (like GET, POST, PUT, DELETE).

## Repository Pattern 

The Repository pattern is a design pattern in which data access logic is isolated in a separate layer, the repository, 
and the rest of the application can access this data through a well-defined API. 
This enables the application to remain agnostic about the underlying data storage.

The key features of the Repository pattern are:

Abstraction of Data Access: The repository provides an abstraction layer between the data access and the business logic 
layer of the application. This abstraction makes it easier to separate concerns, which simplifies the application and makes it easier to maintain.

Testability: Because the data access logic is isolated, it's easier to write unit tests for this functionality. 
You can use mock objects instead of the real database for testing.

Loose Coupling: The repository pattern helps to achieve loose coupling between the layers of the application. 
The application doesn't need to know about the underlying database or data access technology.

Centralization of Data Access Code: With the Repository pattern, data access logic is centralized in one place.
This can help to avoid code duplication and makes it easier to change the data source or the technology used for data access.

The blueprints (resources) in this project represent the logical implementation of the repository pattern. 

### Tips & Tricks

Use Flask-RESTful: Flask-RESTful is an extension for Flask that adds support for quickly building REST APIs. 
It is a lightweight abstraction that works with your existing ORM/libraries.

Error Handling: Create a common error handling mechanism to return consistent error responses.

Request Validation: Validate incoming data with libraries like marshmallow. It will save you a lot of work in the long run.

Use Blueprints: Flask's blueprint module allows you to group related routes, so it's a good way to organize your code as your application grows.

Testing: Flask has a lot of nice features that make testing your application easy. Don't forget to write tests!

Secure your API: Considerations include rate limiting, using tokens, and setting up proper CORS.

Use a good WSGI server: While Flask's built-in server is good for development, it's not suitable for production. 
You'll need to set up a more robust WSGI server for deploying your application. We will be using gunicorn

Manage your Database Connection: Use SQLAlchemy ORM for managing database connections and transactions. 
SQLAlchemy is a SQL toolkit and Object-Relational Mapping (ORM) system for Python, which gives application 
developers the full power and flexibility of SQL.

Documentation: Swagger UI is a great tool to create beautiful, interactive API documentation.

Modularize Your Code: As your application grows, split your routes and other code into multiple f
iles/modules. This will make your code easier to understand and maintain.

For more detailed information, you should look at the official Flask documentation (https://flask.palletsprojects.com/) 
and Flask-RESTful documentation (https://flask-restful.readthedocs.io/).

## Waitress WSGI Server

Waitress is intended to be a production-quality pure-Python WSGI server with very acceptable performance. 
It has no dependencies except ones that live in the Python standard library.

## Unit Tests

Unit tests should be run from the root folder. Unit tests can be run from bash as follows: 

```
python -m pytest tests/attributes_tests.py
```

## Performance Tests 

Performance testing can be done using JMeter. 

Instructions for getting started with JMeter: 
https://www.simplilearn.com/tutorials/jmeter-tutorial/jmeter-api-testing

## Usage

Accessing swagger: /api/swagger-ui

Named Insured Endpoint: /named_insured

Validation Endpoint: /validate

## Adding new APIs & datable objects to the project 

1. MODELS: 
    Add a new model file, and name the file as <database_object_MODEL>.py. Column mapping can be manually added, or implicitly imported via:
    __table__ = Table(__tablename__, MetaData(bind=db.engine), autoload=True, autoload_with=db.engine)

2. SCHEMAS:
    Create a new schema file, and name the file as <database_object_SCHEMA>.py. Validation rules (that will apply to the API) should be explicitly mapped out. 

3. RESOURCES:
    The API interface, that maps to the abstracted database object, will be defined as a blueprint in a resource file. 
    The file should be named as <database_object_BLUEPRINT>.py

4. AGGREGATIONS:
    The aggregations folder contains logic related to the data aggregation processes (including Pandas)

5. If necessary, add new libraries used to the requirements.txt file 

## Git Stuff

cd existing_repo

git remote add origin <ADD>

git branch -M main

git push -uf origin main
