from marshmallow import Schema, fields, validate
from marshmallow_sqlalchemy import SQLAlchemyAutoSchema
from models import TblXrefWkshtCfrmCovgModel

'''
The TblXrefWkshtCfrmCovgSchema class in this code is a subclass of SQLAlchemyAutoSchema, 
which means it's designed to automatically generate a Marshmallow schema based on the TblXrefWkshtCfrmCovgModel SQLAlchemy model.

The model = TblXrefWkshtCfrmCovgModel line inside the Meta class tells SQLAlchemyAutoSchema 
that it should base its schema on the TblXrefWkshtCfrmCovgModel.

oad_instance = True line, if uncommented, would instruct Marshmallow 
to create instances of TblXrefWkshtCfrmCovgModel when deserializing data. This is useful if you're 
loading data and you want it to be automatically turned into TblXrefWkshtCfrmCovgModel instances.

Defining a serialization schema for a TblXrefWkshtCfrmCovgModel model.
'''

# TblXrefWkshtCfrmCovgschema 
class TblXrefWkshtCfrmCovgSchema(SQLAlchemyAutoSchema):
    class Meta:
        model = TblXrefWkshtCfrmCovgModel
        # load_instance = True  # Optional: deserialize to model instances
