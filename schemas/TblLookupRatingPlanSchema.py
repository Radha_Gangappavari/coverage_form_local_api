from marshmallow import Schema, fields, validate
from marshmallow_sqlalchemy import SQLAlchemyAutoSchema
from models import TblLookupRatingPlanModel

'''
The TblLookupRatingPlanSchema class in this code is a subclass of SQLAlchemyAutoSchema, 
which means it's designed to automatically generate a Marshmallow schema based on the TblLookupRatingPlanModel SQLAlchemy model.

The model = TblLookupRatingPlanModel line inside the Meta class tells SQLAlchemyAutoSchema 
that it should base its schema on the TblLookupRatingPlanModel.

oad_instance = True line, if uncommented, would instruct Marshmallow 
to create instances of TblLookupRatingPlanModel when deserializing data. This is useful if you're 
loading data and you want it to be automatically turned into TblLookupRatingPlanModel instances.

Defining a serialization schema for a TblLookupRatingPlanModel model.
'''

# TblLookupRatingPlanSchema 
class TblLookupRatingPlanSchema(SQLAlchemyAutoSchema):
    class Meta:
        model = TblLookupRatingPlanModel
        # load_instance = True  # Optional: deserialize to model instances