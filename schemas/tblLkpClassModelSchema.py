from marshmallow import Schema, fields, validate
from marshmallow_sqlalchemy import SQLAlchemyAutoSchema
from models import TblLookupClassModel

''' 
The main reason for this file is to create a schema for the attributes table.
This is used for serialization/deserialization of the attributes table.
This makes it easier to work with the attributes table in the code.
'''

# This is using the marshmallow_sqlalchemy library for serialization/deserialization
class TblLkpClassSchema(SQLAlchemyAutoSchema):
    class Meta:
        model = TblLookupClassModel
        # load_instance = True  # Optional: deserialize to model instances