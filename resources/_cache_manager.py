# from load_environment import *  # Import the load_environment module from the root directory
import redis
import json
import hashlib
from resources._logger import CustomLogger
import os
from typing import Dict

from dotenv import load_dotenv
load_dotenv()

logger = CustomLogger("Coverga Form", "logs/app.log")
redis_url = os.environ["REDIS_URL"]
# redis_url = "redis://dataelasticachecluster-dev.sy8xjv.ng.0001.use1.cache.amazonaws.com:6379"
print(redis_url) 
logger.log("info", f"Redis URL: {redis_url}")


try:
    cache = redis.Redis.from_url(redis_url, socket_timeout=0.5)
    print(cache)
    logger.log("info", "Successfully connected to Redis.")
except redis.ConnectionError:
    cache = None
    logger.log("error", "Redis Connection Error")
    raise
except redis.TimeoutError:
    cache = None
    logger.log("error", "Redis Timeout Error")
    raise
except Exception as e:
    cache = None
    logger.log("error", f"An unexpected error occurred: {e}")
    raise


print("cache: ", cache)

def safe_cache_get(cache_key):
    try:
        return cache.get(cache_key)
    except (redis.exceptions.ConnectionError, redis.exceptions.TimeoutError):
        logger.log(
            "warning", "Redis connection failed. Skipping cache (safe_cache_get)."
        )
        return None


def safe_cache_set(cache_key, value):
    try:
        cache.set(cache_key, json.dumps(value))
    except (redis.exceptions.ConnectionError, redis.exceptions.TimeoutError):
        logger.log(
            "warning", "Redis connection failed. Skipping cache (safe_cache_set)."
        )

@staticmethod
def get_cache_key_dict(request_args: Dict[str, str]) -> str:
    # Generate a cache key based on the request arguments
    return "cache_{}".format(
        hashlib.md5(str(request_args).encode("utf-8")).hexdigest()
        )


def get_cache_key(request):
    return "cache_{}".format(hashlib.md5(request.url.encode("utf-8")).hexdigest())


def safe_cache_flushall():
    try:
        cache.flushall()
    except redis.exceptions.ConnectionError:
        logger.log("warning", "Redis connection failed. Skipping cache flush.")
