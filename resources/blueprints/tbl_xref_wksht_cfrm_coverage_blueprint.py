from models import TblXrefWkshtCfrmCovgDataClass as TblXrefWkshtCfrmCovgModel
from schemas import TblXrefWkshtCfrmCovgSchema as TblXrefWkshtCfrmCovgSchema
from ._abstract_base_class import BaseResource 

# import the flask framework
from flask import jsonify, request
from flask.views import MethodView
from flask_smorest import Blueprint, abort
from datetime import datetime
import sys
import os
from sqlalchemy import and_, or_, not_, text
import json

from dotenv import load_dotenv

load_dotenv()


sys.path.append("..")  # Add the parent directory (root) to the system path

model_var = TblXrefWkshtCfrmCovgModel
schema_var = TblXrefWkshtCfrmCovgSchema

# define the blueprint for RtgDataClass
tblxref_blp = Blueprint("Xref class", "Xref class")


def generic_get_query_filter(model, identifier_field, **kwargs):
    conditions = []
    identifier_value = kwargs.get(identifier_field, None)

    if identifier_value:
        conditions.append(getattr(model, identifier_field) == identifier_value)

    return and_(*conditions) if conditions else None


@tblxref_blp.route("/tblxref/<string:identifier>", methods=["GET", "PUT", "POST"])
class RatingclassById(BaseResource):
    model = model_var
    schema = schema_var
    identifier_field = "num_ref_class"  # Field used to identify records

    def get_query_filter(self, **kwargs):
        return generic_get_query_filter(self.model, self.identifier_field, **kwargs)


@tblxref_blp.route("/tblxref", methods=["GET", "POST"])
class Ratingclass(BaseResource):
    model = model_var
    schema = schema_var
    identifier_field = "num_ref_class"  # Field used to identify records

    def get_query_filter(self, **kwargs):
        return generic_get_query_filter(self.model, self.identifier_field, **kwargs)


@tblxref_blp.route("/tblxref_test", methods=["GET"])
class RatingAttributeTest(BaseResource):
    def get_query_filter(self, **kwargs):
        """
        No query filter required for the test endpoint.
        """
        return None  # For the purpose of a test, no filter conditions needed

    def get(self, **kwargs):
        """
        Validate that code stream is reaching this blueprint.
        """
        return jsonify({"message": "tblxref Rating Test Endpoint Work"}), 200