from src.db import db
from resources._logger import CustomLogger
# from load_environment import *  # Import the load_environment module from the root directory
from .._cache_manager import (
    safe_cache_get,
    safe_cache_set,
    get_cache_key,
    safe_cache_flushall,
)
from ._error_handler import handle_db_error

# import the flask framework
from flask import jsonify, request
from flask.views import MethodView
from flask_smorest import Blueprint, abort
from datetime import datetime
import sys
import os
from sqlalchemy.exc import SQLAlchemyError, IntegrityError, DataError
import json
from abc import ABC, abstractmethod

sys.path.append("..")  # Add the parent directory (root) to the system path

from dotenv import load_dotenv
load_dotenv()

# create a logger object
logger = CustomLogger("Coverga Form", "logs/app.log")

class BaseResource(MethodView, ABC):
    model = None  # To be overridden by subclass
    schema = None  # To be overridden by subclass

    @abstractmethod
    def get_query_filter(self, **kwargs):
        """Generate filters for SQLAlchemy query. To be implemented by subclass."""
        pass

    def get(self, **kwargs):
        # Extract any query parameters from the URL
        query_params = request.args.to_dict()

        # Generate SQLAlchemy filter conditions based on query parameters
        filter_conditions = self.get_query_filter(**query_params)

        cache_key = get_cache_key(request)
        response = safe_cache_get(cache_key)

        if response is not None:
            return json.loads(response)

        query = self.model.query
        if filter_conditions is not None:
            query = query.filter(filter_conditions)

        records = query.all()

        # If only one record, you might want to handle it differently
        if len(records) == 1:
            records = records[0]

        try:
            response = self.schema().dump(records, many=(isinstance(records, list)))
            safe_cache_set(cache_key, response)
            return response
        except (IntegrityError, SQLAlchemyError, Exception, DataError) as e:
            handle_db_error(e)
        finally:
            db.session.close()

    def put(self, identifier):
        request_data = request.get_json()

        try:
            # Use the identifier_field to filter
            item = self.model.query.filter_by(**{self.identifier_field: identifier}).first()
            if not item:
                abort(404, message=f"{self.model.__name__} not found")

            # Update item based on key-value pairs in request_data
            for key, value in request_data.items():
                if hasattr(item, key):
                    setattr(item, key, value)
                    
            db.session.commit()
            safe_cache_flushall()
            return jsonify("success"), 201
        except (IntegrityError, SQLAlchemyError, Exception, DataError) as e:
            handle_db_error(e)
        finally:
            db.session.close() 


    def post(self, event=None):
        request_data = request.get_json()
        if not request_data:
            abort(400, message="Bad Request: No data received")

        # Initialize an empty object of the given model
        new_object = self.model()

        # Dynamically assign attributes based on request data
        for key, value in request_data.items():
            if hasattr(new_object, key):
                setattr(new_object, key, value)
            else:
                abort(400, message=f"Invalid attribute: {key}")

        try:
            db.session.add(new_object)
            db.session.commit()
            safe_cache_flushall()
        except (IntegrityError, SQLAlchemyError, DataError) as e:
            handle_db_error(e)
        finally:
            db.session.close()

        return jsonify("success"), 201
    
    def get_test(self, **kwargs):
        """
        Validate that code stream is reaching this blueprint
        """
        logger.log("info", "get_test() called")
        return jsonify("success"), 201
