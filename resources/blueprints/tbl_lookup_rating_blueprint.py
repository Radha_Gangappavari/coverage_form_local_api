from models import TblLookupRatingPlanModel as TblLookupRatingPlanModel
from schemas import TblLookupRatingPlanSchema as TblLookupRatingPlanSchema
from ._abstract_base_class import BaseResource 

# import the flask framework
from flask import jsonify, request
from flask.views import MethodView
from flask_smorest import Blueprint, abort
from datetime import datetime
import sys
import os
from sqlalchemy import and_, or_, not_, text
import json

from dotenv import load_dotenv

load_dotenv()


sys.path.append("..")  # Add the parent directory (root) to the system path

model_var = TblLookupRatingPlanModel
schema_var = TblLookupRatingPlanSchema

# define the blueprint for RtgDataClass
tbllookuprating_blp = Blueprint("LookupRating Plan", "Rating Plan")


def generic_get_query_filter(model, cde_rating_plan, **kwargs):
    conditions = []
    identifier_value = kwargs.get(cde_rating_plan, None)

    if identifier_value:
        conditions.append(getattr(model, cde_rating_plan) == identifier_value)

    return and_(*conditions) if conditions else None


@tbllookuprating_blp.route("/tblLookuprating/<string:identifier>", methods=["GET", "PUT", "POST"])
class tbllookupratingById(BaseResource):
    model = model_var
    schema = schema_var
    cde_rating_plan = "num_ref_class"  # Field used to identify records

    def get_query_filter(self, **kwargs):
        return generic_get_query_filter(self.model, self.cde_rating_plan, **kwargs)


@tbllookuprating_blp.route("/tblLookuprating", methods=["GET", "POST"])
class tbllookuprating(BaseResource):
    model = model_var
    schema = schema_var
    cde_rating_plan = "num_ref_class"  # Field used to identify records

    def get_query_filter(self, **kwargs):
        return generic_get_query_filter(self.model, self.cde_rating_plan, **kwargs)


@tbllookuprating_blp.route("/tbllookuprating_test", methods=["GET"])
class tbllookupratingTest(BaseResource):
    def get_query_filter(self, **kwargs):
        """
        No query filter required for the test endpoint.
        """
        return None  # For the purpose of a test, no filter conditions needed

    def get(self, **kwargs):
        """
        Validate that code stream is reaching this blueprint.
        """
        return jsonify({"message": "tbllookuprating Test Endpoint Work"}), 200