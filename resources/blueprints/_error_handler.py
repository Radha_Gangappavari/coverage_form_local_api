
from flask_smorest import abort
from src.db import db
from resources._logger import CustomLogger

# create a logger object
logger = CustomLogger("Coverga Form", "logs/app.log")

def handle_db_error(e):
    db.session.rollback()
    logger.log("error", str(e))
    abort(400, message=str(e))
