"""
VARIABLE VALUES
    model
    schema
    identifier_field
"""

from models import TblLookupClassModel
from schemas import TblLkpClassSchema
from ._abstract_base_class import BaseResource

# import the flask framework
from flask import jsonify, request
from flask.views import MethodView
from flask_smorest import Blueprint, abort
import sys
import os
from sqlalchemy import and_, or_, not_, text

sys.path.append("..")  # Add the parent directory (root) to the system path


model_var = TblLookupClassModel
schema_var = TblLkpClassSchema

# define the blueprint for lookupClass
tbl_insurable_schedule_fact_blp = Blueprint(
    "Lookup Class","Lookup Class"
)


def generic_get_query_filter(model, identifier_field, **kwargs):
    conditions = []
    identifier_value = kwargs.get(identifier_field, None)

    if identifier_value:
        conditions.append(getattr(model, identifier_field) == identifier_value)

    return and_(*conditions) if conditions else None


@tbl_insurable_schedule_fact_blp.route(
    "/dallas-standard-alone-rater-api/v1/tbl_insurable_schedule_fact/<string:identifier>", methods=["GET", "PUT", "POST"]
)
class TblInsurableScheduleFactById(BaseResource):
    model = model_var
    schema = schema_var
    identifier_field = "CDE_INSURABLE_SCHEDULE_TEMPLATE"  # Field used to identify records

    def get_query_filter(self, **kwargs):
        return generic_get_query_filter(self.model, self.identifier_field, **kwargs)


@tbl_insurable_schedule_fact_blp.route(
    "/dallas-standard-alone-rater-api/v1/tbl_insurable_schedule_fact", methods=["GET", "POST"]
)
class TblInsurableScheduleFact(BaseResource):
    model = model_var
    schema = schema_var
    identifier_field = "CDE_INSURABLE_SCHEDULE_TEMPLATE"  #  4 total field unique identifier

    def get_query_filter(self, **kwargs):
        return generic_get_query_filter(self.model, self.identifier_field, **kwargs)


@tbl_insurable_schedule_fact_blp.route(
    "/dallas-standard-alone-rater-api/v1/tbl_insurable_schedule_fact_test", methods=["GET"]
)
class TblInsurableScheduleFactTest(BaseResource):
    def get_query_filter(self, **kwargs):
        """
        No query filter required for the test endpoint.
        """
        return None  # For the purpose of a test, no filter conditions needed

    def get(self, **kwargs):
        """
        Validate that code stream is reaching this blueprint.
        """
        return jsonify({"message": "tbl_lookup Endpoint Work"}), 200
