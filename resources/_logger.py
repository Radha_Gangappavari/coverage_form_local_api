import logging


class CustomLogger:
    def __init__(self, logger_name, log_file, level=logging.INFO):
        self.logger = logging.getLogger(logger_name)

        # File handler for logging to a file
        file_handler = logging.FileHandler(log_file)

        # Stream handler for logging to stdout
        stream_handler = logging.StreamHandler()

        formatter = logging.Formatter(
            "%(asctime)s - %(levelname)s - %(name)s - %(message)s"
        )
        file_handler.setFormatter(formatter)
        stream_handler.setFormatter(formatter)  # You can use the same formatter

        self.logger.addHandler(file_handler)
        self.logger.addHandler(stream_handler)  # Add the stream handler
        self.logger.setLevel(level)

    def log(self, level, msg):
        if level.lower() == "info":
            self.logger.info(msg)
        elif level.lower() == "warning":
            self.logger.warning(msg)
        elif level.lower() == "error":
            self.logger.error(msg)
        elif level.lower() == "critical":
            self.logger.critical(msg)
        else:
            self.logger.debug(msg)
