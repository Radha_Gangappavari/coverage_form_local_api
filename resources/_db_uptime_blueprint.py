###################################
## BLUEPRINT FOR database uptime ##
###################################
# from load_environment import *  # Import the load_environment module from the root directory
from src.db import db
from resources._logger import CustomLogger

# import the flask framework
from flask import jsonify, request, g
from flask.views import MethodView
from flask_smorest import Blueprint, abort
from sqlalchemy import text
from sqlalchemy.exc import SQLAlchemyError
import sys
import os

from dotenv import load_dotenv

load_dotenv()

sys.path.append("..")  # Add the parent directory (root) to the system path

# create cache object
from cachetools import LRUCache, cached

cache = LRUCache(maxsize=100)  # Maximum size of cache 100 entries

db_uptime_blp = Blueprint("Check Database is Up", "Check Database is Up")


class CheckUptime(MethodView):
    @cached(cache)
    def get(self):
        """
        Validate that code stream is reaching this blueprint
        run a direct sql statement to get sysdate
        """
        try:
            result = db.session.execute(text("select sysdate from dual")).fetchone()
            cache["sysdate"] = result[0].strftime("%Y-%m-%d %H:%M:%S")
            return jsonify({"last update": cache["sysdate"]}), 200
        except (SQLAlchemyError, Exception) as e:
            if "sysdate" in cache:
                return jsonify({"last sysdate recorded": cache["sysdate"]}), 200
            return jsonify({"message": "Database is down"}), 500
        finally:
            db.session.close()


db_uptime_blp.add_url_rule("/coverage/v1/uptime", view_func=CheckUptime.as_view("uptime"))
