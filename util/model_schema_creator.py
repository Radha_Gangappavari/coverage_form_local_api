import os
import logging
from jinja2 import Environment, FileSystemLoader, TemplateNotFound

# Define the path for templates, output directories and resources directory
root_dir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
template_dir = os.path.join(root_dir, "util/templates")
models_dir = os.path.join(root_dir, "models")
schemas_dir = os.path.join(root_dir, "schemas")
resources_dir = os.path.join(root_dir, "resources")

# Create a log file in the util directory
log_file = os.path.join(template_dir, "template.log")

# Initialize logger
logging.basicConfig(
    filename=log_file,
    level=logging.INFO,
    format="%(asctime)s:%(levelname)s:%(message)s",
)
logger = logging.getLogger(__name__)

# Check if output directories exist, if not create them
os.makedirs(models_dir, exist_ok=True)
os.makedirs(schemas_dir, exist_ok=True)
os.makedirs(resources_dir, exist_ok=True)

# Create the jinja2 environment
env = Environment(loader=FileSystemLoader(template_dir))

# Read table names from domain_tables file
with open(os.path.join(template_dir, "domain_tables.txt"), "r") as file:
    tables = [line.strip() for line in file]

# Loop through each table
for table in tables:
    model_file_path = os.path.join(models_dir, f"{table}.py")
    schema_file_path = os.path.join(schemas_dir, f"{table}_schema.py")
    blueprint_file_path = os.path.join(resources_dir, f"{table}_blueprint.py")

    # Check if files already exist
    if (
        not os.path.exists(model_file_path)
        and not os.path.exists(schema_file_path)
        and not os.path.exists(blueprint_file_path)
    ):
        try:
            # Load the templates
            model_template = env.get_template("model_template.txt")
            schema_template = env.get_template("schema_template.txt")
            blueprint_template = env.get_template("blueprint_template.txt")

            # Render the templates with table name and write to files
            model_file_content = model_template.render(name=table)
            with open(model_file_path, "w") as f:
                f.write(model_file_content)

            schema_file_content = schema_template.render(name=table)
            with open(schema_file_path, "w") as f:
                f.write(schema_file_content)

            blueprint_file_content = blueprint_template.render(name=table)
            with open(blueprint_file_path, "w") as f:
                f.write(blueprint_file_content)

            # Render import statements for __init__.py in models, schemas, and resources directories
            init_model_template = env.get_template("init_model_template.txt")
            init_schema_template = env.get_template("init_schema_template.txt")
            init_resource_template = env.get_template("init_resources_template.txt")

            init_model_file_content = init_model_template.render(name=table)
            init_schema_file_content = init_schema_template.render(name=table)
            init_resource_file_content = init_resource_template.render(name=table)

            # Append import statements to __init__.py files
            with open(os.path.join(models_dir, "__init__.py"), "a") as f:
                f.write("\n" + init_model_file_content + "\n")

            with open(os.path.join(schemas_dir, "__init__.py"), "a") as f:
                f.write("\n" + init_schema_file_content + "\n")

            with open(os.path.join(resources_dir, "__init__.py"), "a") as f:
                f.write("\n" + init_resource_file_content + "\n")

            logger.info(f"Created model, schema, and blueprint for table: {table}")

        except TemplateNotFound:
            logger.error(f"Templates not found in the directory {template_dir}")
        except Exception as e:
            logger.error(f"An error occurred: {str(e)}")
    else:
        logger.info(f"Model, schema or blueprint already exists for table: {table}")
