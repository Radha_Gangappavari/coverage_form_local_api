# use function whenever you want to convert date to a properly formatted date
from datetime import datetime


def convert_date(cls, v):
    formatted_date = datetime.strptime(cls, "%Y-%m-%d %H:%M:%S")
    v = formatted_date.strftime("%Y-%m-%d %H:%M:%S")
    # return v
    try:
        return v
    except ValueError:
        raise ValueError("Incorrect data format, should be YYYY-MM-DD HH:MM:SS")
